# homebin

Collection of shell scripts which helps me everyday.

Homebin it is next generation of my previous separate git repos debian-bin, fedora-bin, mac-bin repo.

In this version you abble to create you own collection by fetching or combining the right branch into you prefered path.

You need to checkout homebin.git into your ~/bin folder. Then switch to your prefer os-related branch and merge all neseserry scripts using `homebin` command.

Main branch (OS currently in use) with recent updates is [Debian](/../../tree/debian). 

# homebin.d

Script collection has multiple different set of scripts. They are separated by purpose:

## Initial fresh system configuration:

Every time when you install fresh linux (debian, ubuntu, fedora) you run 1 command to setup all your prefered packages and install neccesery drivers and set /etc/ into your liking.

```ls
cdebian                cdebian-desktop
```

## Basics usefull scripts

Simple scripts helps you to see processor memory usage, warmup your keyboard (by loading CPU with 100%) or run text editor from command line and so on...

```ls
psmem  text  warmup
```

## Encryption

If you upload or backup your scripts or personal files into cloud, better to encrypt it using encfs and only then upload to cloud. Then mount and unmount the encrypted directory with simple set of scripts:

```ls
encfscfgyandexdisk
encfscfgdropbox
encfscfggoogledrive
```

## Games

If you play wine games on linux, you have to watch for recent wine updates, periodically checking dxvk and storing games saves into your cloud directory.

```ls
homebin enable homebin.d/gog/quake2
homebin enable homebin.d/*
encfscfg-save ~/bin/quake2
winehere --list-lutris
winehere --list-winehq
```

# How to install homebin

First checkout the following tree:

* For **[Debian](/../../tree/debian)**
* For [Ubuntu](/../../tree/ubuntu)
* For [Mac](/../../tree/mac)
* For [Windows](/../../tree/windows)
